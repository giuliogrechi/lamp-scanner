Giulio Grechi
Sistemi di acquisizione dati - aa 18/19

Il software misura la corrente che scorre attraverso
una lampadina, collegata ad un generatore programmabile via protocollo GPIB.
Vengono eseguite misure cadenzate a partire dall'istante in cui è impostata 
la tensione sul generatore. Successivamente la tensione viene variata all'interno
del range fornito dall'utente e con un passo preimpostato. La 
scansione viene eseguita con step di tensioni crescenti e decrescenti.

I parametri impostabili dall'utente sono i seguenti:

-l      uscita del generatore a cui è collegata la lampada
-i      indirizzo del device (generatore)
-t      durata della misura per una tensione fissata, in secondi
-c      cadenza delle misure per una tensione fissata, in secondi
-s      step nell'incremento o decremento della tensione in Volt
-v      tensione massima da impostare nella rampa in Volt
-m      tensione minima da impostare nella rampa in Volt
-f      path del file di salvataggio dati
-g      visualizzazione dei grafici
-h      visualizzazione dell'help

Esempio:

./lamp_scanner -l 3 -t 10 -c 1 -s 0.5 -v 12 -m 0 -f log.dat -g 
in questo caso viene effettuata una misura del canale 3, la tensione 
viene variata tra 0 e 12V a passi di 0.5V. Ogni tensione viene mantenuta 
per 10 secondi, misurando la corrente ogni secondo. Infine nel file log.dat vengono registrati 
i risultati della misura. Il comando -g abilita la visualizzazione dei grafici 
temporali della corrente per ogni tensione impostata.

Esempio di file di log

# indirizzo periferica 6
# numero lampada: 3 
# durata della misura: 20000000 us
# cadenza 500000 us
# V max 2.000 V
# V min 0.000 V
# step 50 mV
# numero misure per ogni valore di tensione 41 
# lettura temperatura 22.13
# u/d: 0 misura con step di tensione crescente - 1 misura con step di tensione decrescente - 2 valore asintotico della corrente, calcolato
# usec: misurati a partire dall'istante (circa...) in cui è avvenuta l'impostazione della tensione
# v_set: tensione impostata via software
# v_read: tensione effettiva impostata dal generatore
# corrente: corrente misurata dall'amperometro del generatore
#
# u/d  usec        v_set       v_read     corrente
0      4141 0.000000e+00 4.000000e-03 2.400000e-03
0    506390 0.000000e+00 4.000000e-03 2.400000e-03
0   1005454 0.000000e+00 4.000000e-03 2.400000e-03
0   1506947 0.000000e+00 4.000000e-03 2.400000e-03
0   2005937 0.000000e+00 4.000000e-03 2.400000e-03
...
1      4194 0.000000e+00 4.000000e-03 2.400000e-03
1    505576 0.000000e+00 4.000000e-03 2.400000e-03
1   1004571 0.000000e+00 4.000000e-03 2.400000e-03
1   1507173 0.000000e+00 4.000000e-03 2.400000e-03
1   2005748 0.000000e+00 4.000000e-03 2.400000e-03
...
2         0 5.000000e-02 5.500000e-02 4.185000e-02
0      4350 1.000000e-01 1.050000e-01 8.070000e-02
0    506855 1.000000e-01 1.050000e-01 7.910000e-02

Analisi dati

Per una caratterizzazione completa dell'oggetto di studio è necessario
effettuare due misure: una in un intervallo di tensione limitato e step ridotto
(valori ragionevoli 1V 0.05V) ed una in un intervallo più ampio e step più grandi 
(es 12V 0.5V).
Utilizzando i primi dati, vengono effettuati dei fit con una funzione di tipo quadratico,
sui valori I V e utilizzando un numero decrescente di punti, riducendo il limite
superiore della tensione, fino al raggiungere un minimo 
del chi quadro ridotto. Dai parametri di questo fit si riesce a ricavare la
resistenza a temperatura ambiente e il fattore geometrico g della lampada.
Con questi valori e insieme ai dati raccolti dalla seconda misura (oltre alla 
temperatura ambiente, misurata anch'essa dal software),
è possibile calcolare la relazione temperatura/potenza della 
lampadina.

Uno script bash permette di automatizzare la procedura, richiamando uno 
script gnuplot appositamente preparato.

uso:
./read_file.sh g_factor_file.dat acquisition_file.dat temperatura fitplot.gp 

esempio:
./start.sh
che eseque il comando:
./read_file.sh fattoreG_l1.dat acquisizione_l1.dat 22.21 fitplot.gp

Un'analisi completa delle misure sulle quattro lampade a disposizione può 
essere visualizzata utilizzando lo script gnuplot gnu_gfac.gp all'interno 
della cartella esempio_analisi.
