##############################################
#              Giulio Grechi                 #
#   Corso di Sistemi di acquisizione dati    #
#                                            #
#                                            #
##############################################
set term qt 2 position 400,400;
unset key;
set xlabel 'Tempo [us]';
set ylabel 'Corrente [A]';
set title 'Andamento temporale corrente, step 0.2V';
set label 1  sprintf("0.6V->0.4V") at graph 0.70,0.18;
set label 2  sprintf("0.2V->0.4V") at graph 0.70,0.24;
set label 3  sprintf("0.8V->0.6V") at graph 0.70,0.34;
set label 4  sprintf("0.4V->0.6V") at graph 0.70,0.40;
set label 5  sprintf("0.6V->0.8V") at graph 0.55,0.45;
set label 6  sprintf("1.0V->0.8V") at graph 0.55,0.50;
set label 7  sprintf("0.8V->1.0V") at graph 0.70,0.53;
set label 8  sprintf("1.2V->1.0V") at graph 0.70,0.58;
set label 9  sprintf("1.0V->1.2V") at graph 0.55,0.61;
set label 10 sprintf("1.4V->1.2V") at graph 0.55,0.65;
plot [0:1e7] 'acquisizione_lamp3_tot804_time.dat' \
                                    using (($1==1 && $3==4e-1)  ? $2 : 1/0):5 \
                                    with lines linecolor 1,\
             'acquisizione_lamp3_tot804_time.dat' \
                                    using (($1==0 && $3==4e-1)  ? $2 : 1/0):5 \
                                    with lines linecolor 1,\
             'acquisizione_lamp3_tot804_time.dat' \
                                    using (($1==1 && $3==6e-1)  ? $2 : 1/0):5 \
                                    with lines linecolor 2,\
             'acquisizione_lamp3_tot804_time.dat' \
                                    using (($1==0 && $3==6e-1)  ? $2 : 1/0):5 \
                                    with lines linecolor 2,\
             'acquisizione_lamp3_tot804_time.dat' \
                                    using (($1==1 && $3==8e-1)  ? $2 : 1/0):5 \
                                    with lines linecolor 3,\
             'acquisizione_lamp3_tot804_time.dat' \
                                    using (($1==0 && $3==8e-1)  ? $2 : 1/0):5 \
                                    with lines linecolor 3,\
             'acquisizione_lamp3_tot804_time.dat' \
                                    using (($1==1 && $3==6e-1)  ? $2 : 1/0):5 \
                                    with lines linecolor 4,\
             'acquisizione_lamp3_tot804_time.dat' \
                                    using (($1==0 && $3==6e-1)  ? $2 : 1/0):5 \
                                    with lines linecolor 4,\
             'acquisizione_lamp3_tot804_time.dat' \
                                    using (($1==1 && $3==10e-1) ? $2 : 1/0):5 \
                                    with lines linecolor 5,\
             'acquisizione_lamp3_tot804_time.dat' \
                                    using (($1==0 && $3==10e-1) ? $2 : 1/0):5 \
                                    with lines linecolor 5,\
             'acquisizione_lamp3_tot804_time.dat' \
                                    using (($1==1 && $3==12e-1) ? $2 : 1/0):5 \
                                    with lines linecolor 6,\
             'acquisizione_lamp3_tot804_time.dat' \
                                    using (($1==0 && $3==12e-1) ? $2 : 1/0):5 \
                                    with lines linecolor 6,\
             'acquisizione_lamp3_tot804_time.dat' \
                                    using (($1==1 && $3==14e-1) ? $2 : 1/0):5 \
                                    with lines linecolor 7,\
             'acquisizione_lamp3_tot804_time.dat' \
                                    using (($1==0 && $3==14e-1) ? $2 : 1/0):5 \
                                    with lines linecolor 7,\
             'acquisizione_lamp3_tot804_time.dat' \
                                    using (($1==1 && $3==16e-1) ? $2 : 1/0):5 \
                                    with lines linecolor 8,\
             'acquisizione_lamp3_tot804_time.dat' \
                                    using (($1==0 && $3==16e-1) ? $2 : 1/0):5 \
                                    with lines linecolor 8,\
             'acquisizione_lamp3_tot804_time.dat' \
                                    using (($1==1 && $3==18e-1) ? $2 : 1/0):5 \
                                    with lines linecolor 9,\
             'acquisizione_lamp3_tot804_time.dat' \
                                    using (($1==0 && $3==18e-1) ? $2 : 1/0):5 \
                                    with lines linecolor 9;             
set label 1  ""; set label 2  ""; set label 3  "";
set label 4  ""; set label 5  ""; set label 6  "";
set label 7  ""; set label 8  ""; set label 9  ""; set label 10 "";

#
#temperature misurate durante le 4 acquisizioni dati
t1=24.10 + 273.15;
t2=24.97 + 273.15;
t3=24.83 + 273.15;
t4=25.00 + 273.15;

#coefficienti per la conversione resistività-temperatura
wa1 = -3.28353;  wa2 = 0.0264003; wa3 = 1.79125e-6
wa4 = -3.238;    wa5 = 0.0263579; wa6 = 1.80302e-06
wa7 = -0.724112; wa8 = 0.0191576; wa9 = 6.88698e-06

wb1 = 207.697; wb2 = 33.7132; wb3 = -0.0366286
wb4 = 133.745; wb5 = 36.2054; wb6 = -0.0581687
wb7 = 49.5998; wb8 = 46.4533; wb9 = -0.368427

# rs(x): resistivita' (microohm*cm) vs. temperatura (kelvin)
rs(x) = x > 1900 ?  wa1 + wa2*x + wa3*x*x : \
	x > 800 ? wa4 + wa5*x + wa6*x*x : wa7 + wa8*x + wa9*x*x;

# ts(x): temperatura (kelvin) vs. resistivita' (microohm*cm)
ts(x) = x > 53.35 ? wb1 + wb2*x + wb3*x*x : \
  	x > 19.00 ? wb4 + wb5*x + wb6*x*x : wb7 + wb8*x + wb9*x*x;

rho1 = rs(t1)*1e-6;
rho2 = rs(t2)*1e-6;
rho3 = rs(t3)*1e-6;
rho4 = rs(t4)*1e-6;


set term qt 0 position 0,0;
set fit prescale;
q(x) = a1*x*x+b1*x+c1;
w(x) = a2*x*x+b2*x+c2;
r(x) = a3*x*x+b3*x+c3;
t(x) = a4*x*x+b4*x+c4;
set fit errorvariables
fit q(x) 'datitognuplot_gfac_g_factor_lamp1.dat' via a1,b1,c1;
chi_sq_1 = sqrt(FIT_WSSR)
fit w(x) 'datitognuplot_gfac_g_factor_lamp2.dat' via a2,b2,c2;
chi_sq_2 = sqrt(FIT_WSSR)
fit r(x) 'datitognuplot_gfac_g_factor_lamp3.dat' via a3,b3,c3;
chi_sq_3 = sqrt(FIT_WSSR)
fit t(x) 'datitognuplot_gfac_g_factor_lamp4.dat' via a4,b4,c4;
chi_sq_4 = sqrt(FIT_WSSR)

R1=1/b1;
R2=1/b2;
R3=1/b3;
R4=1/b4;
R1_err = (b1_err/(b1*b1)); 
R2_err = (b2_err/(b2*b2));
R3_err = (b3_err/(b3*b3));
R4_err = (b4_err/(b4*b4));

g1 = rho1/R1;
g1_err = g1*rho1*R1_err/R1;
g2 = rho2/R2;
g2_err = g2*rho2*R2_err/R2;
g3 = rho3/R3;
g3_err = g3*rho3*R3_err/R3;
g4 = rho4/R4;
g4_err = g4*rho4*R4_err/R4;

unset key;
set xlabel 'Tensione [V]';
set ylabel 'Corrente [A]';
set title 'Corrente vs Tensione';   
set label 1 sprintf("R1: %.4f pm %.3f ohm\tg1: %2.3f(x10^-6 cm)",\
                                           R1,R1_err,g1*1e6) at graph 0.05,0.95;
set label 2 sprintf("R2: %.4f pm %.3f ohm\tg2: %2.3f(x10^-6 cm)",\
                                           R2,R2_err,g2*1e6) at graph 0.05,0.90;
set label 3 sprintf("R3: %.4f pm %.3f ohm\tg3: %2.3f(x10^-6 cm)",\
                                           R3,R3_err,g3*1e6) at graph 0.05,0.85;
set label 4 sprintf("R4: %.4f pm %.3f ohm\tg4: %2.3f(x10^-6 cm)",\
                                           R4,R4_err,g4*1e6) at graph 0.05,0.80;


plot q(x),w(x),r(x),t(x),'datitognuplot_gfac_g_factor_lamp1.dat',\
                         'datitognuplot_gfac_g_factor_lamp2.dat',\
                         'datitognuplot_gfac_g_factor_lamp3.dat',\
                         'datitognuplot_gfac_g_factor_lamp4.dat';


set term qt 1 position 200,200;;

f(x) = A*(x**a- t1**a) + B*((x-t1)**b);
g(x) = C*(x**c- t2**c) + D*((x-t2)**d);
h(x) = E*(x**e- t3**e) + F*((x-t3)**f);
i(x) = G*(x**g- t4**g) + H*((x-t4)**h);
A=1e-13;B=1e-3;C=1e-13;D=1e-3;E=1e-13;F=1e-3;G=1e-13;H=1e-3;
a=4;c=4;e=4;g=4;b=1;d=1;f=1;h=1;

set fit errorvariables
fit f(x) 'datitognuplot_acquisizione_lamp1_tot804.dat' via a,b,A,B;
chi_sq_1 = 1000*sqrt(FIT_WSSR)
fit g(x) 'datitognuplot_acquisizione_lamp2_tot804.dat' via c,d,C,D;
chi_sq_2 = 1000*sqrt(FIT_WSSR)
fit h(x) 'datitognuplot_acquisizione_lamp3_tot804.dat' via e,f,E,F;
chi_sq_3 = 1000*sqrt(FIT_WSSR)
fit i(x) 'datitognuplot_acquisizione_lamp4_tot804.dat' via g,h,G,H;
chi_sq_4 = 1000*sqrt(FIT_WSSR)

unset key;
set ylabel 'Potenza [W]';
set xlabel 'Temperatura [K]';
set title 'Potenza vs Temperatura'

set label 1 sprintf("a1: %.3f pm %.3f\tb1: %.3f pm %.3f\tchi: %.0f mW",\
                                   a,a_err,b,b_err,chi_sq_1) at graph 0.05,0.95;
set label 2 sprintf("a2: %.3f pm %.3f\tb2: %.3f pm %.3f\tchi: %.0f mW",\
                                   c,c_err,d,d_err,chi_sq_2) at graph 0.05,0.90;
set label 3 sprintf("a3: %.3f pm %.3f\tb3: %.3f pm %.3f\tchi: %.0f mW",\
                                   e,e_err,f,f_err,chi_sq_3) at graph 0.05,0.85;
set label 4 sprintf("a4: %.3f pm %.3f\tb4: %.3f pm %.3f\tchi: %.0f mW",\
                                   g,g_err,h,h_err,chi_sq_4) at graph 0.05,0.80;

plot f(x), 'datitognuplot_acquisizione_lamp1_tot804.dat',\
     g(x), 'datitognuplot_acquisizione_lamp2_tot804.dat',\
     h(x), 'datitognuplot_acquisizione_lamp3_tot804.dat',\
     i(x), 'datitognuplot_acquisizione_lamp4_tot804.dat';

print '';
print sprintf("R1: %.4f pm %.3f ohm\tg1: %2.4f(x10^-6 cm)",R1,R1_err,g1*1e6);
print sprintf("R2: %.4f pm %.3f ohm\tg2: %2.4f(x10^-6 cm)",R2,R2_err,g2*1e6);
print sprintf("R3: %.4f pm %.3f ohm\tg3: %2.4f(x10^-6 cm)",R3,R3_err,g3*1e6);
print sprintf("R4: %.4f pm %.3f ohm\tg4: %2.4f(x10^-6 cm)",R4,R4_err,g4*1e6);
print '';
print sprintf("a1: %.3f pm %.3f\tb1: %.3f pm %.3f\tchi: %.0f mW",\
                                                      a,a_err,b,b_err,chi_sq_1);
print sprintf("a2: %.3f pm %.3f\tb2: %.3f pm %.3f\tchi: %.0f mW",\
                                                      c,c_err,d,d_err,chi_sq_2);
print sprintf("a3: %.3f pm %.3f\tb3: %.3f pm %.3f\tchi: %.0f mW",\
                                                      e,e_err,f,f_err,chi_sq_3);
print sprintf("a4: %.3f pm %.3f\tb4: %.3f pm %.3f\tchi: %.0f mW",\
                                                      g,g_err,h,h_err,chi_sq_4);

