#!/bin/bash

while IFS= read -r line; do
  if [ "${line:0:1}" == "2" ]
  then
    echo $line >> TEMPavg_values.dat
  fi
done < "$1"

NUMLINES=$(wc -l < TEMPavg_values.dat)
echo `gnuplot -p -e "filename='TEMPavg_values.dat'; t=$3; lines = $NUMLINES; file_g = '$1'; file_acq = '$2'" $4`
echo `rm TEMPavg_values.dat`