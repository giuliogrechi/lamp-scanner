set term qt 0 noenhanced position 0,0;

#t è un parametro passato da riga di comando, conversione in K
t1=t + 273.15;

#coefficienti per la conversione resistività-temperatura
wa1 = -3.28353;  wa2 = 0.0264003; wa3 = 1.79125e-6
wa4 = -3.238;    wa5 = 0.0263579; wa6 = 1.80302e-06
wa7 = -0.724112; wa8 = 0.0191576; wa9 = 6.88698e-06

wb1 = 207.697; wb2 = 33.7132; wb3 = -0.0366286
wb4 = 133.745; wb5 = 36.2054; wb6 = -0.0581687
wb7 = 49.5998; wb8 = 46.4533; wb9 = -0.368427

# rs(x): resistivita' (microohm*cm) vs. temperatura (kelvin)
rs(x) = x > 1900 ?  wa1 + wa2*x + wa3*x*x : \
	x > 800 ? wa4 + wa5*x + wa6*x*x : wa7 + wa8*x + wa9*x*x;

# ts(x): temperatura (kelvin) vs. resistivita' (microohm*cm)
ts(x) = x > 53.35 ? wb1 + wb2*x + wb3*x*x : \
  	x > 19.00 ? wb4 + wb5*x + wb6*x*x : wb7 + wb8*x + wb9*x*x;


rho1 = rs(t1)*1e-6;

set fit prescale;
#set fit quiet;
q(x) = a1*x*x+b1*x+c1;
set fit errorvariables
chi = 1;
bestfit = 0;

#lines è un parametro passato da riga di comando
do for [i=0:lines-5]{
    len = lines - i;
    fit q(x) filename using 4:5 every ::::len via a1,b1,c1;
    chi_sq_1 = sqrt(FIT_WSSR)

    R1=1/b1;
    R1_err = (b1_err/(b1*b1));

    g1 = rho1/R1;
    g1_err = g1*rho1*R1_err/R1;

    if (FIT_STDFIT < chi){ 
        chi = FIT_STDFIT;
        bestfit = len;
        a_temp=a1;
        b_temp=b1;
        c_temp=c1;
        g=g1;
        g_err=g1_err;
        R=R1;
        R_err = (b1_err/(b1*b1));
        };
}
a1=a_temp;
b1=b_temp;
c1=c_temp;
unset key;
set xlabel 'Tensione [V]';
set ylabel 'Corrente [A]';
set title 'Corrente vs Tensione';   
set label 1 sprintf("R: %.4f pm %.3f ohm\tg: %2.3f(x10^-6 cm)",\
                                            R,R_err,g*1e6) at graph 0.05,0.95;
#set label 2 sprintf("%s",file_g) at graph 0.05,0.85;
plot q(x), filename using 4:5 every ::::bestfit;

set term qt 1 noenhanced position 100,100;
f(x) = A*(x**a- t1**a) + B*((x-t1)**b);
A=1e-13;B=1e-3;
a=4.1;b=1.01;

set fit errorvariables
fit f(x) file_acq using \
                  (($1==2)  ? (ts(g*(($4/$5)*1e6))) : 1/0):($4*$5) via a,b,A,B;
chi_sq_1 = 1000*sqrt(FIT_WSSR)

unset key;
set ylabel 'Potenza [W]';
set xlabel 'Temperatura [K]';
set title 'Potenza vs Temperatura'
unset xrange;
unset yrange;

set label 1 sprintf("a: %.3f pm %.3f\tb: %.3f pm %.3f\tchi: %.0f mW",\
                                   a,a_err,b,b_err,chi_sq_1) at graph 0.05,0.95;
#set label 2 sprintf("%s",file_acq) at graph 0.05,0.85;

plot file_acq using (($1==2)  ? (ts(g*(($4/$5)*1e6))) : 1/0):($4*$5),f(x);

print '';
print sprintf("R1: %.4f pm %.3f ohm\tg: %2.4f(x10^-6 cm)",R,R_err,g*1e6);
print sprintf("a: %.3f pm %.3f\tb: %.3f pm %.3f\tchi: %.0f mW",\
                                                      a,a_err,b,b_err,chi_sq_1);