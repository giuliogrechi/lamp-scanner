/**
 *   ##############################################
 *   #              Giulio Grechi                 #
 *   #   Corso di Sistemi di acquisizione dati    #
 *   #                                            #
 *   #   Misura della corrente attraverso una     #
 *   #       lampada, al variare del tempo        #
 *   #         e della tensione applicata         #
 *   ##############################################
*/

#include  <stdio.h>
#include  <stdlib.h>
#include  <gpib/ib.h>
#include  <limits.h>
#include  <errno.h>
#include  <string.h>
#include  <unistd.h>
#include  <sys/types.h>
#include  <sys/stat.h>
#include  <fcntl.h>
#include  <sys/time.h>
#include  <math.h>
#include  <signal.h>


#define BOARD 0
#define EOI 1                               //assert eoi line on write
#define EOS 0x40A                           //End of string gpib device
#define TIMEOUT_OPEN_DEV 11

/**maximum ratings*/
#define I_MAX_A 2.0
#define V_MAX_LIMIT_VOLT 12
#define V_MIN_LIMIT_VOLT 0

#define ADDRESS_TEMPERATURE_DEVICE 24
#define TIMEOUT_LOCK_SEC 2
#define SIZE_CH_BUF 120

/**
 * valori di default utilizzati, in assenza di opzioni impostate da riga di 
 * comando
 */
#define ADDRESS 6
#define LAMP_N 2
#define TO_WAIT_USEC 10000000
#define CADENCE_USEC (10000000/2)
#define V_MAX_VOLT 12
#define V_MIN_VOLT 0
#define STEP_mVOLT 100 //in mV
#define DATA_FILE "log.dat"

/**
 * variabile globale per la gestione del file descriptor del device
 * ibdev ritorna un numero > 0 se l'apertura è andata a buon fine, -1 se
 * qualcosa è andato storto.
 */
static int g_dev_d = -2;
static int g_lamp_n;

/**
 * struttura che contiene tutti i parametri utilizzati durante l'acquisizione
 */
struct arguments
{
   int address;
   int address_temperature_dev;
   int lamp_number;
   double v_max_volt;
   double v_min_volt;
   int measure_lenght_usec;
   int cadence_usec;
   int num_voltage_values;
   int step_mVolt;
   char path_data_file[100];
   int num_meas_todo_vfix;
   int flag_graph;
   char lock_path[60];
};

/**
 * per liberare le risorse impegnate da dev
 */
void close_dev(dev)
{
    ibonl(dev,0);
    return;
}

void reset_dev(dev)
{
    ibonl(dev,1);
    return;
}

/**
 * se l'acquisizione fallisce in qualche sua parte, per esempio nell'invio
 * e ricezione dei comandi, nel locking/unlocking del file associato alla
 * periferica, oppure alla pressione di ctrl-c, la procedura viene interrotta
 * inviando un segnale, e questa funzione ne è il gestore.
 * Si occupa di spegnere la lampadina se era accesa, di chiudere il device
 * se era aperto e di uscire.
 */
void sighandler(int sig)
{
    if(sig == SIGINT){
        printf("\nKeyboard interrupt %d\n", sig);
    }
    if(sig == SIGTERM){
        printf("\nSIGTERM  %d\n", sig);
    }
    if(g_dev_d != -2){
        printf("Spegnimento lampada\n");
        char buf_send[SIZE_CH_BUF];
        sprintf(buf_send, "vset %d,0",g_lamp_n);
        /**
         * la successiva istruzione di write sul dispositivo non sempre
         * va a buon fine. In particolare capita che venga raramente
         * restituito un errore
         * ibutil.c:384: conf_lock_board: Assertion `conf->has_lock == 0' failed
         * in questo caso la libreria esegue un abort che non permette
         * alla lampada di spegnersi correttamente.
         * il problema si verifica anche chiudendo e riaprendo il device o
         * effettuando un reset del file descriptor prima
         * di dare l'istruzione di write. Andrebbe verificato se esiste un
         * metodo per controllare il locking della scheda,
         * (ibsta&LOK) non si riferisce a questo caso.
         */
        ibwrt(g_dev_d,buf_send,strlen(buf_send));
        if(ibsta&ERR){
            printf("Errore nello spegnimento della lampada"\
                                     "- impostazione tensione %s\n",buf_send);
        }else{
            printf("Lampada spenta\n");
        }

        printf("Chiusura del device\n");
        close_dev(g_dev_d);
    }
    printf("Uscita dal programma\n");
    raise(sig);
    exit(2);
}

int parse_string_to_integer(char argom[])
{
    errno = 0;
    int device = strtol(argom, NULL, 10);
    if ((errno == ERANGE && (device == LONG_MAX || device == LONG_MIN))
                         || (errno != 0 && device == 0)){

        printf("Errore nel parsing dell'intero: %s\n",argom);
        raise(SIGTERM);
    }
  return device;
}

/**
 * Converte un array di char in una stringa, rimuovendo gli eventuali
 * caratteri \n o \r, e terminando opportunamente con 0
 * Segue una conversione in un float
 */
double parse_string_to_float(int num_byte_read, char *data)
{
    double result_float = 0;
    if(num_byte_read == 0){

        printf("Sono stati letti 0 bytes\n");
        raise(SIGTERM);
    }

    while (num_byte_read > 0 && (data[num_byte_read - 1] == '\n'
                                 || data[num_byte_read - 1] == '\r')){
        num_byte_read--;
    }
    if(num_byte_read == SIZE_CH_BUF){

        printf("Sono stati letti %d bytes, il buffer e' pieno\n",SIZE_CH_BUF);
        data[SIZE_CH_BUF] = 0;
        printf("byte letti: %s\n",data);
        raise(SIGTERM);
    }else if(num_byte_read < SIZE_CH_BUF){

        errno = 0;
        data[num_byte_read] = 0;
        result_float = strtod(data,NULL);
        if ((errno == ERANGE &&
                      (result_float == HUGE_VAL || result_float == -HUGE_VAL))
                      || (errno != 0 && result_float == 0)){

            printf("Errore nel parsing del float: %s - result: %f\n",
                                                          data, result_float);
            raise(SIGTERM);
        }
    }
    return result_float;
}

/**
 * imposta la corrente massima sul dispositivo identificato da dev
 */
int set_max_current(int dev, struct arguments ags)
{
    char buf_send[SIZE_CH_BUF];
    sprintf(buf_send, "iset %d,%.3f", ags.lamp_number,I_MAX_A);
    ibwrt(dev,buf_send,strlen(buf_send));
    if(ibsta&ERR){
        printf("Errore nell'invio del comando - impostazione max corrente %s\n",
                                                                      buf_send);
        return -1;

    }
    printf("impostata corrente massima: %.3f A\n",I_MAX_A);
    return 0;
}

/*
 * R(T) = Rz * (1 + a * T + b * T * T) + Rs
 * funzione di conversione per la resistenza al platino
 */
double resistance_to_temperature(double r)
{
    float a = 3.90802e-3;
    float b = -5.80195e-7;
    float rs = 0.561556;
    float rz = 99.9105;
    float rn;

    rn = (r - rs)/rz;                            /* normalized resistance */
    return (-a + sqrt(a*a - 4*b*(1-rn)))/(2*b);
}

/**
 * Restituisce il valore della temperatura, in double, misurata attraverso una
 * resistenza al platino,collegata ad un multimetro di indirizzo address_device.
 */
double read_temperature(int address_device)
{
    double temperature = -1;
    errno = 0;
    int dv = -1;
    dv = ibdev(BOARD,address_device,0,TIMEOUT_OPEN_DEV,EOI,EOS);
    if(dv ==-1){

        printf("Errore nell'apertura della comunicazione "\
                                                   "- lettura temperatura\n");
        return -1;
    }

    char buf[SIZE_CH_BUF];
    ibrd(dv,buf,SIZE_CH_BUF);

    if(ibsta&ERR){
        printf("Errore nella lettura dei dati - lettura temperatura\n");
        close_dev(dv);
        return -1;
    }

    temperature = parse_string_to_float(ibcnt, buf);
    temperature = resistance_to_temperature(temperature);
    if(temperature == -1){
        printf("Errore nella lettura della temperatura");
    }
    close_dev(dv);
    return temperature;
}

/**
 * Lock_file e unlock_file semplificano la gestione del locking, in questo
 * caso particolare.
 * da ricordare che lockf è bloccante e se la risorsa è occupata non dà errore
 * ma aspetta fino a quando la risorsa non ritorna disponibile.
 * Il lock è rilasciato automaticamente quando il file descriptor viene chiuso,
 * quindi nell'eventualità che l'acquisizione fallisca e il programma esca il
 * locking viene rilasciato automaticamente.
 */
int lock_file(char path[])
{
    int fd = open(path,O_RDWR);
    if(fd < 0){

        printf("errore nell'apertura del file di lock\n");
        return -1;
    }
    int status = lockf(fd,F_LOCK,0);
    if(status != 0){

        printf("errore nell'ottenere il lock della periferica\n");
        return -1;
    }
    return fd;
}

int unlock_file(int f_descr)
{
    int status = lockf(f_descr,F_ULOCK,0);

    if(status != 0){
        printf("errore nel rilasciare il lock della periferica\n");
        return -1;
    }

    status = close(f_descr);
    if(status != 0){
        printf("errore nel chiudere il file di lock\n");
        return -1;
    }
    return 0;
}

/**
 * test del file di lock per verificare che qualcuno non stia impegnando
 * continuativamente a lungo la risorsa
 */
int test_lock_file(char path[], int timeout_lock)
{
    int test_flock = -1;
    unsigned int elapsed;

    struct timeval lst;
    struct timeval strt;

    int fd = open(path,O_RDWR);
    if(fd < 0){
        printf("errore nell'apertura del file di lock\n");
        return -1;
    }

    gettimeofday(&strt,NULL);
    while(test_flock != 0){

        test_flock = lockf(fd,F_TEST,0);

        gettimeofday(&lst,NULL);
        elapsed = (lst.tv_sec - strt.tv_sec);
        if(elapsed > timeout_lock){
            printf("Un altro processo sta tenendo impegnata la periferica "\
                                      "per più di %d secondi\n",timeout_lock);
            return -2;
        }
    }

    int status = close(fd);
    if(status != 0){
        printf("errore nel chiudere il file di lock\n");
        return -1;
    }
    return 0;
}

/**
 * Stampa a schermo le opzioni che verranno utilizzate dal software
 * durante l'acquisizione dei dati
 */
void print_options_sent(struct arguments ag)
{
  printf("\t####opzioni impostate dall'utente####\n"\
         "indirizzo periferica %d\n"\
         "numero lampada: %d \n"\
         "durata della misura con V fissata: %d us\n"\
         "cadenza %d us\n"\
         "numero misure per ogni valore di tensione %d \n"\
         "V max %.3lf V\n"\
         "V min %.3lf V\n"\
         "Step %d mV\n"\
         "path del file di salvataggio dei dati\t%s\n"\
         "path del file di lock\t%s\n"\
         "\t########\n",
         ag.address,
         ag.lamp_number,
         ag.measure_lenght_usec,
         ag.cadence_usec,
         ag.num_meas_todo_vfix,
         ag.v_max_volt,
         ag.v_min_volt,
         ag.step_mVolt,
         ag.path_data_file,
         ag.lock_path
         );
  return;
}

/**
 * imposta tutte le opzioni, salvate nella struttura di tipo arguments, ai
 * valori di default, assegnati tramite #define
 */
void set_arguments_to_default(struct arguments *ag)
{
    ag->address = ADDRESS;
    ag->address_temperature_dev = ADDRESS_TEMPERATURE_DEVICE;
    ag->lamp_number = LAMP_N;
    g_lamp_n = LAMP_N;
    ag->measure_lenght_usec = TO_WAIT_USEC;
    ag->cadence_usec = CADENCE_USEC;
    ag->v_max_volt = V_MAX_VOLT;
    ag->v_min_volt = V_MIN_VOLT;
    ag->step_mVolt = STEP_mVOLT;
    ag->flag_graph = 0;
    strncpy(ag->path_data_file, DATA_FILE, sizeof(ag->path_data_file));
    return;
}


void print_command_help()
{
    printf("Opzioni da impostare [default]\n");

    printf("-l\tnumero della lampada [%d] \n",LAMP_N);

    printf("-i\tindirizzo del device [%d]\n",ADDRESS);

    printf("-t\tdurata della misura per una tensione fissata,"\
                                       " in secondi [%d us]\n",TO_WAIT_USEC);
    printf("-c\tcadenza delle misure per una tensione impostata,"\
                                       " in secondi [%d us]\n",CADENCE_USEC);
    printf("-s\tstep nell'incremento o decremento della tensione"\
                                       " in Volt [%d mV]\n",STEP_mVOLT);
    printf("-v\ttensione massima da impostare nella rampa"\
                                       " in Volt [%d V]\n",V_MAX_VOLT);
    printf("-m\ttensione minima da impostare nella rampa"\
                                       " in Volt [%d V]\n",V_MIN_VOLT);
    printf("-f\tpath del file di salvataggio dati"\
                                       " [%s]\n",DATA_FILE);
    printf("-g\tvisualizzazione dei grafici I(t)\n");
    return;
}

/**
 * scrive nel data_file le opzioni contenute nella struttura arguments
 */
void print_header_file(FILE *data_file,struct arguments ag,double t_a)
{
    fprintf(data_file, "# indirizzo periferica %d\n",
                                               ag.address);
    fprintf(data_file, "# numero lampada: %d \n",
                                               ag.lamp_number);
    fprintf(data_file, "# durata della misura: %d us\n",
                                               ag.measure_lenght_usec);
    fprintf(data_file, "# cadenza %d us\n",ag.cadence_usec);

    fprintf(data_file, "# V max %.3lf V\n",ag.v_max_volt);

    fprintf(data_file, "# V min %.3lf V\n",ag.v_min_volt);

    fprintf(data_file, "# Step %d mV\n",ag.step_mVolt);

    fprintf(data_file, "# numero misure per ogni valore di tensione %d \n",
                                               ag.num_meas_todo_vfix);

    fprintf(data_file, "# lettura temperatura %.2lf\n",t_a);

    fprintf(data_file, "# u/d: 0 misura con Step di tensione crescente - "\
                           "1 misura con Step di tensione decrescente - "\
                           "2 valore asintotico della corrente, calcolato\n");

    fprintf(data_file, "# usec: misurati a partire dall'istante (circa...)"\
                        " in cui è avvenuta l'impostazione della tensione\n");

    fprintf(data_file, "# v_set: tensione impostata via software\n");

    fprintf(data_file, "# v_read: tensione effettiva impostata dal generatore\n");

    fprintf(data_file, "# corrente: corrente misurata dall'amperometro "\
                                                          "del generatore\n");
    fprintf(data_file, "#\n"\
                       "# u/d  usec        v_set       v_read     corrente\n");
    return;
}

/**
 * interpreta le opzioni da riga di comando, salvando i dati ottenuti
 * all'interno di una struttura arguments. Viene anche calcolato e salvato
 * il numero totale di misure da effettuare per ogni Step di tensione
 *
 * sono presenti alcuni controlli sui dati
 */
void store_user_arguments(int ac, char *av[], struct arguments *argm)
{
    int opt;
    while ((opt = getopt(ac, av, "i:l:t:c:v:m:s:f:gh")) != -1) {
        switch (opt) {
        case 'f': //path del file di salvataggio delle misure
            strncpy(argm->path_data_file, optarg, sizeof(argm->path_data_file));
            break;
        case 'i': //indirizzo della periferica
            argm->address = parse_string_to_integer(optarg);
            break; //numero del canale a cui è connessa la lampada
        case 'l':
            argm->lamp_number = parse_string_to_integer(optarg);
            g_lamp_n = argm->lamp_number;
            break;
        case 't': //durata della singola misura, a tensione fissata,
            argm->measure_lenght_usec = (int)(parse_string_to_float(
                                               strlen(optarg),optarg)*1000000);
            break;
        case 'c': //cadenza, da inserire in secondi, convertita in microsecondi
            argm->cadence_usec = (int)(parse_string_to_float(
                                               strlen(optarg),optarg)*1000000);
            break;
        case 'v': //tensione massima da impostare in volt
            argm->v_max_volt = parse_string_to_float(strlen(optarg), optarg);
            if(argm->v_max_volt > V_MAX_LIMIT_VOLT){

                printf("la tensione massima non può essere maggiore "\
                                            "di %dV - impostati %dV\n",
                                            V_MAX_LIMIT_VOLT,V_MAX_LIMIT_VOLT);
                argm->v_max_volt = V_MAX_LIMIT_VOLT;
            }
            break;
        case 'm': //tensione minima da impostare in volt
            argm->v_min_volt = parse_string_to_float(strlen(optarg), optarg);
            if(argm->v_min_volt < V_MIN_LIMIT_VOLT){

                printf("la tensione minima non può inferiore "\
                                            "a %dV - impostati %dV\n",
                                            V_MIN_LIMIT_VOLT,V_MIN_LIMIT_VOLT);
                argm->v_min_volt = V_MIN_LIMIT_VOLT;
            }
            break;
        case 's': //Step di tensione in millivolt
            argm->step_mVolt = (int)(parse_string_to_float(
                                                  strlen(optarg),optarg)*1000);
            break;
        case 'g': //flag per la visualizzazione o meno dei grafici temporali
            argm->flag_graph = 1;
            break;
        case 'h':
            print_command_help();
            exit(0);
        case '?':
            printf("parametro non riconosciuto\n\n");
            print_command_help();
            exit(0);
        default: /* '?' */
            printf("Non è stato impostato nessun argomento");
        } //end switch(opt)
    } //end while(opt)

    if(argm->v_max_volt <= argm->v_min_volt){
        printf("la tensione massima impostata è inferiore o "\
                                                   "uguale a quella minima\n");
        raise(SIGTERM);
    }
    if( (argm->v_max_volt - argm->v_min_volt)*1000 <= argm->step_mVolt){
        printf("la differenza tra le tensioni massima e minima è inferiore "\
                                                      "allo Step impostato\n");
        raise(SIGTERM);
    }
    if( argm->measure_lenght_usec <= argm->cadence_usec){
        printf("la durata di ogni misura è inferiore alla cadenza "\
                                                                "impostata\n");
        raise(SIGTERM);
    }
    /**
     * il numero totale di misure da effettuare per ogni tensione sarà dato
     * dalla durata totale della misura, diviso la cadenza, +1. Il numero deve
     * essere un intero, la durata totale si può discostare dal valore impostato
     * (ma i valori sono in microsecondi, quindi tale effetto può
     * essere considerato trascurabile)
     */
    argm->num_meas_todo_vfix = argm->measure_lenght_usec/argm->cadence_usec + 1;
    /**
     * calcolo del numero di valori di tensione che dovranno essere impostati.
     * a partire dai valori impostati minimi e massimo, e dallo Step
     */
    argm->num_voltage_values = (int)(
            ((argm->v_max_volt - argm->v_min_volt)*1000)/argm->step_mVolt + 1);

    /**
     * il numero totale di misure è dato dal prodotto del numero di valori
     * di tensione da impostare per il numero di misure a tensione fissata.
     * viene stimata anche la durata totale della misura
     */
    int num_totale_misure = argm->num_voltage_values * argm->num_meas_todo_vfix;
    int total_lenght = ((2*argm->num_voltage_values - 1)
                         * argm->measure_lenght_usec)/1000000;
    double total_lenght_min = (double)total_lenght/60;

    printf("numero di Step di tensione %d\n"\
           "numero totale di misure %d\n"\
           "durata stimata della misura %d s -> %.1lf min\n",
            argm->num_voltage_values,
            num_totale_misure,
            total_lenght,
            total_lenght_min);

    sprintf(argm->lock_path, "/var/lock/gpib/%1.0d.%1.0d",
                                              argm->address,argm->lamp_number);
    return;
}

/**
 * apre una pipe attraverso la quale viene avviato gnuplot, per mostrare il
 * grafico della caratteristica IV della lampadina,
 */
void show_graph_IV(struct arguments agm, double *v_set, double *c_avg)
{
    FILE * gnuplotPipe = popen ("gnuplot -persistent", "w");
    fprintf(gnuplotPipe, "set title 'Caratteristica I(V) \n");
    fprintf(gnuplotPipe, "set xlabel 'tensione [V]' \n");
    fprintf(gnuplotPipe, "set ylabel 'corrente [A]' \n");
    fprintf(gnuplotPipe, "set xrange [0:%.3lf] \n",agm.v_max_volt);
    fprintf(gnuplotPipe, "plot '-'\n");
    int v;
    for(v=1;v<agm.num_voltage_values-1;v++){
        fprintf(gnuplotPipe, "%e %e\n", v_set[v],
                                        c_avg[v]);
    }
    fprintf(gnuplotPipe, "e\n");
    fflush(gnuplotPipe);
    pclose(gnuplotPipe);

    return;
}

/**
 * apre una pipe attraverso la quale viene aperto gnuplot e
 * mostrato l'andamento temporale della corrente che scorre attraverso
 * la lampadina, per una tensione v fissata
 */
void show_graph_It(struct arguments ags,
                   unsigned long int (*u_seconds)[ags.num_voltage_values][2],
                   double (*currents)[ags.num_voltage_values][2],
                   int v, double *volts,
                   int fdown)
{
    FILE * gnuplotPipe = popen ("gnuplot -persistent", "w");
    if(fdown == 0){
        fprintf(gnuplotPipe, "set title 'Tensione impostata %.3lf -> %.3lf'\n",
                                                            volts[v-1],
                                                            volts[v]);
    }
    if(fdown == 1){
        fprintf(gnuplotPipe, "set title 'Tensione impostata %.3lf -> %.3lf'\n",
                                                            volts[v+1],
                                                            volts[v]);
    }
    fprintf(gnuplotPipe, "set xlabel 'tempo [us]' \n");
    fprintf(gnuplotPipe, "set ylabel 'corrente [A]' \n");
    fprintf(gnuplotPipe, "set yrange [0:2] \n");
    fprintf(gnuplotPipe, "set xrange [0:%ld] \n",
                            u_seconds[ags.num_meas_todo_vfix-1][v][fdown]);
    fprintf(gnuplotPipe, "plot '-' \n");
    int g;
    for(g=0;g<ags.num_meas_todo_vfix;g++){
        fprintf(gnuplotPipe, "%ld %e\n", u_seconds[g][v][fdown],
                                          currents[g][v][fdown]);
    }
    fprintf(gnuplotPipe, "e\n");
    fflush(gnuplotPipe);
    pclose(gnuplotPipe);

    return;
}

/**
 * salva i dati acquisiti e contenuti negli array u_seconds, currents, volts,
 * volts_set, curr_avg su un file
 * La prima colonna contiene l'indicazione se la misura è stata effettuata con
 * Step di tensione crescente (0), decrescente (1), o se è il valore calcolato
 * asintotico (2). La seconda colonna contiene il tempo trascorso in microsec
 * dall'istante in cui è stata impostata la tensione (0 se valore asintotico).
 * La terza colonna contiene la tensione inviata via sofware al generatore e
 * la quarta la tensione effettivamente impostata dal generatore.
 * La quinta contiene la corrente letta.
 * Non vengono salvati i valori asintotici per la tensione massima e minima,
 * visto che in tal caso non possono essere eseguiti i due Step di tensione.
 * I dati sono ordinati secondo tensione crescente
 */
void save_data_to_file(struct arguments agm, 
                       double t_amb,
                       unsigned long int (*u_seconds)[agm.num_voltage_values][2],
                       double (*currents)[agm.num_voltage_values][2],
                       double *volts, double *volts_set, double *curr_avg){
    int g;
    int v;
    int flag_down;
    FILE *fdata;
    fdata = fopen(agm.path_data_file, "w");
    if(fdata == NULL){
        printf("Errore nell'apertura del file di log\n");
        return;
    }else{

        print_header_file(fdata,agm,t_amb);
        for(v=0;v<agm.num_voltage_values;v++){

            for(flag_down=0;flag_down<2;flag_down++){

                for(g=0;g<agm.num_meas_todo_vfix;g++){

                    if(!(flag_down == 1 && v == agm.num_voltage_values - 1)){

                        fprintf(fdata, "%01d %9ld %e %e %e\n",
                                                    flag_down,
                                                    u_seconds[g][v][flag_down],
                                                    volts[v],
                                                    volts_set[v],
                                                    currents[g][v][flag_down]);
                    }
                } /** end loop g */
            }/** end loop flag_down */
            if((v<agm.num_voltage_values-1) && v != 0){
                /*
                 * il primo e l'ultimo valore di tensione non sono significativi,
                 * le correnti non sono misurate con doppio Step di tensione
                 */
                fprintf(fdata,"2 %9d %e %e %e\n",0,volts[v],
                                                   volts_set[v],
                                                   curr_avg[v]);
            }
        }/** end loop v */
    }

    fclose(fdata);
    return;
}

int time_diff_avg(struct timeval *last,
                  struct timeval *before,
                  struct timeval *start)
{

    int diff_1 = (before->tv_sec - start->tv_sec)*1000000
               + (before->tv_usec - start->tv_usec);

    int diff_2 = (last->tv_sec - start->tv_sec)*1000000
               + (last->tv_usec - start->tv_usec);

    return (diff_1 + diff_2)*0.5;
}

/**
 * il vettore voltages deve contenere tutte le tensioni che dovranno
 * essere impostate nel dispositivo. Qui vengono calcolate
 */
void fill_voltages_to_set(double *vec, struct arguments ag)
{
    int j;
    vec[0] = ag.v_min_volt;
    for(j=1;j<ag.num_voltage_values;j++){
        /**
         * Lo Step di tensione è salvato in millivolt, ed è un intero.
         * Il dispositivo richiede un valore in Volt
         */
        vec[j] = vec[j-1] + (double)ag.step_mVolt/1000;
    }
    return;
}

int ask_measure(int dev_descr,
                char *command, char *read,
                struct timeval * one, struct timeval * two, 
                char *lockpath)
{
    int fd = lock_file(lockpath);            /**lock file */
    if(fd<0){
        return -1;
    }

    gettimeofday(one,NULL);
    ibwrt(dev_descr,command,strlen(command));  /**richiesta misura */

    if(ibsta&ERR){
        printf("Errore nell'invio del comando %s\n", command);
        unlock_file(fd);   /* non realmente necessario */
        return -1;
    }

    gettimeofday(two,NULL);

    ibrd(dev_descr,read,SIZE_CH_BUF);         /**lettura risposta */

    int status = unlock_file(fd);                   /**unlock file */
    if(status<0){
        return -1;
    }
    if(ibsta&ERR){
        printf("Errore nella lettura dei dati\n");
        return -1;
    }
    return ibcnt;
}

int main(int argc, char *argv[]){

    /**
     * gestione del segnale di keyboard interrupt e del segnale di terminazione
     * tale segnale viene inviato ogni qual volta un'operazione non è andata
     * a buon fine ed è necessario terminare il programma in modo corretto
     * in particolare riportando lo stato delle uscite del generatore
     * di tensione a 0
     */
    signal(SIGINT, sighandler);
    signal(SIGTERM, sighandler);

    /** Inizializzazione dei parametri da utilizzare per l'acquisizione */
    struct arguments args;
    set_arguments_to_default(&args);
    store_user_arguments(argc,argv,&args);
    print_options_sent(args);

    /** lettura temperatura */
    double temp_amb = read_temperature(args.address_temperature_dev);
    printf("lettura temperatura %.2lf\n",temp_amb);

    /** inizializzazione delle strutture per la gestione del tempo */
    struct timeval *start   = malloc(sizeof(struct timeval));
    struct timeval *before  = malloc(sizeof(struct timeval));
    struct timeval *last    = malloc(sizeof(struct timeval));

    struct timeval *now        = malloc(sizeof(struct timeval));
    struct timeval *time_step  = malloc(sizeof(struct timeval));
    struct timeval *next       = malloc(sizeof(struct timeval));
    struct timeval *delay      = malloc(sizeof(struct timeval));
    time_step->tv_sec = 0;
    time_step->tv_usec = args.cadence_usec;
    int sleep_time;

    /** test del file di lock */
    int test_flock = -1;
    test_flock = test_lock_file(args.lock_path, TIMEOUT_LOCK_SEC);
    if(test_flock != 0){
        raise(SIGTERM);
    }

    /** inizializzazione del device (alimentatore) */
    g_dev_d = ibdev(BOARD,args.address,0,TIMEOUT_OPEN_DEV,EOI,EOS);
    if(g_dev_d ==-1){
        printf("Errore nell'apertura della comunicazione\n");
        raise(SIGTERM);
    }

    /** inizializzazione dei buffer per la comunicazione con il device */
    char buf_read[SIZE_CH_BUF];
    char buf_send[SIZE_CH_BUF];
    
    int byte_read;
    int flag_down = 0;
    int status;

    /** inizializzazione degli array di double che conterranno le misure */
    double *voltages = malloc(args.num_voltage_values*sizeof(double));
    fill_voltages_to_set(voltages, args);

    double *voltages_set = malloc(args.num_voltage_values*sizeof(double));

    double (*currents)[args.num_voltage_values][2] =
                           calloc(args.num_meas_todo_vfix, sizeof(*currents));

    unsigned long int (*u_seconds)[args.num_voltage_values][2] =
                           calloc(args.num_meas_todo_vfix, sizeof(*currents));
    
    /** impostazione della corrente massima */
    status = set_max_current(g_dev_d,args);
    if(status != 0){
        raise(SIGTERM);
    }

    /**
     * Loop che scorre l'indice delle tensioni da applicare.
     * l'incremento o decremento dell'indice viene effettuato alla fine,
     * al verificarsi di opportune condizioni.
     */
    int v;
    int g;
    for(v=0;;){
        
        /**impostazione tensione */
        sprintf(buf_send, "vset %d,%.3f", args.lamp_number,voltages[v]);
        ibwrt(g_dev_d,buf_send,strlen(buf_send));

        if(ibsta&ERR){
              printf("Errore nell'invio del comando "\
                                     "- impostazione tensione %s\n",buf_send);
              raise(SIGTERM);
        }
        printf("\ntensione impostata: %.3f V\n",voltages[v]);

        gettimeofday(start,NULL);
        *next = *start;

        /**inizio misure di corrente */
        for(g=0;g<args.num_meas_todo_vfix;g++){  
            /** loop che scorre l'indice temporale */
            sprintf(buf_send, "iout? %d", args.lamp_number);
            
            byte_read = ask_measure(g_dev_d,buf_send,buf_read,
                                    before,last,args.lock_path);
            if(byte_read < 0){
                raise(SIGTERM);
            }

            /** conversione salvataggio dei dati appena letti negli array */
            u_seconds[g][v][flag_down] = time_diff_avg(last,before,start);
            currents[g][v][flag_down] = parse_string_to_float(byte_read, buf_read);

            /** calcolo del delay di microsecondi */
            timeradd(next,time_step,next);
            gettimeofday(now,NULL);
            timersub(next,now,delay);
            sleep_time = delay->tv_sec*1000000 + delay->tv_usec;

            if(sleep_time  > 0){
                usleep(sleep_time);
            }else{
                printf("Cadenza troppo elevata\n");
                raise(SIGTERM);
            }
        }/**fine loop temporale*/

        /** Lettura della tensione effettivamente impostata dal generatore */
        sprintf(buf_send, "vout? %d", args.lamp_number);
        byte_read = ask_measure(g_dev_d, buf_send,buf_read,
                                NULL,NULL,args.lock_path);
        if(byte_read < 0){
                raise(SIGTERM);
        }

        voltages_set[v] = parse_string_to_float(byte_read, buf_read);
        printf("Tensione letta: %.4lf V\n",voltages_set[v]);

        /**
         * la visualizzazione dell'andamento temporale della corrente avviene
         * solo quando la tensione impostata è diversa da 0 e quando è stata
         * abilitata con l'opzione da riga di comando
         */
        if(voltages[v] != 0.0 && args.flag_graph == 1){
           show_graph_It(args, u_seconds, currents, v, voltages, flag_down);
        }

        /** incremento o decremento dell'indice di tensione */
        if(flag_down == 0){ 
            /** caso Step di tensione crescente */
            printf("salto di tensione %.3lf -> %.3lf \t"\
                   "corrente dopo %ld us: %.4lf A \n",
                    voltages[v-1],
                    voltages[v],
                    u_seconds[args.num_meas_todo_vfix-1][v][flag_down],
                    currents[args.num_meas_todo_vfix-1][v][flag_down]);
            if(v != args.num_voltage_values - 1){
                /**
                 * non è stata ancora raggiunta la tensione massima da applicare,
                 * continua con l'incremento dell'indice del vettore voltages[]
                 */
                v++;
            }else{
                /**
                 * la tensione impostata era quella massima, il ciclo 
                 * continua in senso inverso 
                 */
                flag_down = 1;
                v--;
            }
        }else if (flag_down == 1){
            /** caso Step di tensione crescente */
            printf("salto di tensione %.3lf -> %.3lf \t"\
                   "corrente dopo %ld us: %.4lf A \n",
                    voltages[v+1],
                    voltages[v],
                    u_seconds[args.num_meas_todo_vfix-1][v][flag_down],
                    currents[args.num_meas_todo_vfix-1][v][flag_down]);
            if(v != 0){
                v--;
            }else{
               break;
            }
        }else{
            printf("Errore nell'incremento o decremento della tensione applicata");
            raise(SIGTERM);
        }
    }/** fine loop tensioni */

    /** Calcolo della corrente stazionaria */
    double *currents_avg = malloc(args.num_voltage_values*sizeof(double));
    for(v=0;v<args.num_voltage_values;v++){

        g = args.num_meas_todo_vfix-1;
        currents_avg[v] = (currents[g][v][0] + currents[g][v][1])/2;
        printf("V: %3.lf I_up: %2.4lf I_dw: %2.4lf I_av_ %2.4lf\n",
                voltages[v],
                currents[g][v][0],
                currents[g][v][1],
                currents_avg[v]);
    }

    /** Visualizzazione dei dati di tensione e corrente (asintotica) */
    if(args.num_voltage_values > 2){
        show_graph_IV(args,voltages_set,currents_avg);
    }else{
        printf("pochi punti per poter costruire il grafico\n");
    }

    /** Salvataggio dei dati all'interno del file */
    save_data_to_file(args, temp_amb,
                      u_seconds,
                      currents,
                      voltages, voltages_set, currents_avg);

    /** lettura della temperatura per un eventuale utilizzo successivo */
    printf("lettura temperatura %.2lf\n",temp_amb);

    free(currents);
    free(currents_avg);
    free(u_seconds);
    free(voltages);
    free(voltages_set);

    free(start);
    free(before);
    free(last);
    free(now);
    free(time_step);
    free(next);
    free(delay);

    close_dev(g_dev_d);

    return 0;
}

